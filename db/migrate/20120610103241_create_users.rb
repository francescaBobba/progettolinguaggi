class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.text :training
      t.text :work
      t.text :skill
      t.text :hobby

      t.timestamps
    end
  end
end
