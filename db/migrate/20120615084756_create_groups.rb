class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :admin
      t.string :description

      t.timestamps
    end
    add_index :groups, [:created_at]
  end
end
