module UsersHelper
  
  # Returns the Gravatar (http://gravatar.com/) for a given user
  def gravatar_for(user, options = { size: 50 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
  
  def group_admin_profile(user)
    nomegruppo = Group.where(:admin => user).pluck(:id)
    Group.find(nomegruppo)
  end

  def group_admin(user, group)
    Group.where(:admin => user, :id => group).count
  end
  
  def group_members(group)
        usersid = Groupship.where(:group_id => group).pluck(:user_id)
        User.find(usersid)
  end
  
  def name(id)
    User.find(params[:id])
  end
  
  def groupship(group_id, user_id)
    usersid = Groupship.where(:group_id => group_id)
    userid = usersid.where(:user_id => user_id)
    Groupship.find(userid)
  end

  
end
