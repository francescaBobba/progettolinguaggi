module ApplicationHelper
  # Returns the full title on a per-page basis.
  def titolo_intero(page_title)
    base_title = "CineMates"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end
end
