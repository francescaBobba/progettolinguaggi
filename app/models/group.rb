class Group < ActiveRecord::Base
  attr_accessible :name, :admin, :id, :description
  
  has_many :microposts, dependent: :destroy
  belongs_to :user
  
  validates :admin, presence: true
  validates :name , presence:true
  
  has_many :groupships,  dependent: :destroy
  has_many :users, :through => :groupships
    
  default_scope order: 'groups.created_at DESC' #ordina attravesro la data di creazione che è indice
end
