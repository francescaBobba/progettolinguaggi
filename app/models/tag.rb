class Tag < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :name, :category, :id

  validates :name, presence:true
  validates :category, presence:true
  CATEGORIES = ['Cinema', 'Informatica', '3d', 'Comunicazione']
  
  belongs_to :usertags
  has_many :usertags,  dependent: :destroy

end
