class Message < ActiveRecord::Base
  attr_accessible :sender, :recipient, :id, :subject, :body
  
  is_private_message
  # The :to accessor is used by the scaffolding,
  # uncomment it if using it or you can remove it if not
  #attr_accessor :to
  belongs_to :users
end