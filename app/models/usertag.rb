class Usertag < ActiveRecord::Base
  attr_accessible :tag_id, :user_id
  
  validates :tag_id, presence: true
  validates :user_id , presence: true
  
  belongs_to :user
  belongs_to :tag
  has_many :tags
end
