class Micropost < ActiveRecord::Base
 
  attr_accessible :content, :group_id
  belongs_to :user
  belongs_to :group #associa il micropost all'utente insieme a has_many nel user_model
  
  validates :user_id, presence: true
  validates :group_id, presence:true
  validates :content, presence: true, length: { maximum: 160 }
  
  default_scope order: 'microposts.created_at DESC' #per ordinare i micropost in ordine descescente di creazione
 
end
