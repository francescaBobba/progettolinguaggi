class User < ActiveRecord::Base
  attr_accessible :skill, :training, :hobby, :work, :id, :name, :email, :password
  has_secure_password

  has_private_messages

  has_many :microposts, dependent: :destroy #ogni utente può avere più mivropost, se ut cancellato si cancellano anche i suoi post
  has_many :groups, dependent: :destroy
  has_many :groupships
  has_many :groups, :through => :groupships
  has_many :tags, :through => :usertags #ognuno può avere tanti tag 
  has_many :usertags
  has_many :messages, dependent: :destroy

  before_save { |user| user.email = email.downcase }
  
  before_save :create_remember_token

  validates :name, presence:true, length: { maximum: 50 }
  validates :id, presence:true
  validates :password, length: { minimum: 6 }
  validates :skill, presence:true
  validates :training, presence:true
  validates :hobby, presence:true
  validates :work, presence:true
  validates :work, presence:true
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
    
  def feed
    # This is preliminary.
    Micropost.where("user_id = ?", id)
  end
  
  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%") 
    else
      scoped
    end
  end

private

    def create_remember_token
      # in Ruby 1.8.7 you should use SecureRandom.hex here instead
      self.remember_token = SecureRandom.urlsafe_base64
    end

end