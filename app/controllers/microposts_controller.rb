class MicropostsController < ApplicationController
  before_filter :signed_in_user, only: [:create, :destroy]
  before_filter :correct_user,   only: :destroy
  
  def create
  @micropost = Micropost.new(params[:micropost])
  @micropost.user_id = current_user.id
    if @micropost.save
     # flash[:success] = "Micropost creato con successo!"
      redirect_to(:back) 
    else
      @feed_items = []
      redirect_to groups_path(:id)
    end
  end

  def destroy
    @micropost.destroy
    redirect_to(:back)
  end

  private

    def correct_user
      @micropost = current_user.microposts.find_by_id(params[:id])
      redirect_to root_path if @micropost.nil?
    end
    
end