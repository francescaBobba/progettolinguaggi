class TagsController < ApplicationController
  before_filter :signed_in_user, only: [:create]
  before_filter :correct_user,   only: :destroy

  def show
    @tag = Tag.find(params[:id])
    @user = current_user
  end

  def index
   @user = current_user
   @tags = Tag.all
  end
  
  def create
    @tag = Tag.new(params[:tag])
    if @tag.save
      @usertag = Usertag.new #crea una nuova tupla in usertag con user_id e tag_id
      @usertag.user_id =  current_user.id
      @usertag.tag_id= @tag.id
      @usertag.save
      flash.now[:error] = 'Tag creato!'
      redirect_to tags_path
    else
      flash[:error] = 'Inserisci nome e categoria'
      redirect_to(:back)
    end
  end

  def destroy
    @tag.destroy
    redirect_to tags_path
  end
  
 
   private

  def correct_user
      @tag = current_user.tags.find_by_id(params[:id])
      redirect_to root_path if @tag.nil?
  end 
  
end
