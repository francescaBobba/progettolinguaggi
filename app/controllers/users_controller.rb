class UsersController < ApplicationController
  
  before_filter :signed_in_user, only: [:edit, :update]
  before_filter :correct_user,   only: [:edit, :update]
 
  def show
    @user = User.find(params[:id])
    @usertag=Usertag.all
    @tag=Tag.all
    #mi mostra il profilo dell'utente loggato
  end
  
  def new
    @user = User.new
  end
  #crea la sessione per l'utente che si logga sennò da errore
	def create
      user = User.find_by_email(params[:session][:email])
      if user && user.authenticate(params[:session][:password])
        sign_in user
        redirect_back_or user
      else
        flash.now[:error] = 'Invalid email/password combination'
        render 'new'
      end
  end
  #seleziona l'utente da modificare
  def edit
    @user = User.find(params[:id])
  end
   #per aggiornare le proprietà dell'utente
  def update
    @user = User.find(params[:id])
      @user.update_column(:work, params[:user][:work])
      @user.update_column(:training, params[:user][:training])
      @user.update_column(:skill, params[:user][:skill])
      @user.update_column(:hobby, params[:user][:hobby])
    
      redirect_to @user
  end
  
  def search
    if params[:format].nil?
      @group = nil
      @users = User.search(params[:search]).paginate(:page => params[:page])
    else     
      @group = Group.find(params[:format])
      @users = User.search(params[:search]).paginate(:page => params[:page])
    end
  end
 
  def search_tag
    if !current_user.nil?
    @users = User.find(:all,
                     :joins => "INNER JOIN usertags ON users.id = usertags.user_id
                                INNER JOIN tags on tags.id = usertags.tag_id",
                     :conditions => ["LOWER(tags.name) LIKE ? AND users.id IS NOT ?", params[:name], current_user.id])
    else
    @users = User.find(:all,
                     :joins => "INNER JOIN usertags ON users.id = usertags.user_id
                                INNER JOIN tags on tags.id = usertags.tag_id",
                     :conditions => ["LOWER(tags.name) LIKE ? ", params[:name]])
    end                   
  end
  
  def search_category
    if !current_user.nil?
    @users = User.uniq.find(:all,
                     :joins => "INNER JOIN usertags ON users.id = usertags.user_id
                                INNER JOIN tags on tags.id = usertags.tag_id",
                     :conditions => ["LOWER(tags.category) LIKE ? AND users.id IS NOT ?", params[:category], current_user.id])
    else
          @users = User.uniq.find(:all,
                     :joins => "INNER JOIN usertags ON users.id = usertags.user_id
                                INNER JOIN tags on tags.id = usertags.tag_id",
                     :conditions => ["LOWER(tags.category) LIKE ?", params[:category]])
    end
  end
  
  def messages
    @title = "Messaggi ricevuti"
    @user = User.find(params[:id])
    @messages = @user.received_messages 
  end
  
  private
  
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(@user)
    end
  
    def signed_in_user
      unless signed_in?
        store_location
        redirect_to signin_path, notice: "Please sign in."
    end
  end
end