class MessagesController < ApplicationController
  before_filter :signed_in_user, only: [:create, :destroy]
  before_filter :correct_user,   only: :destroy


  def new
    @message = Message.new
    @user = User.find(params[:user])
    if @message.invite == true
      @group = Group.find(params[:group_id])
    end
   @message.invite = false
  end

  def create
    @message = Message.new
    @message.sender = User.find_by_name(params[:message][:sender])
    @message.recipient = User.find_by_name(params[:message][:recipient])
    @message.subject = params[:message][:subject]
    @message.invite = params[:message][:invite]
    if @message.invite == true
      @group = Group.find(params[:message][:group_id])
      @message.body = @group.id
    else
      @message.body = params[:message][:body]
    end
    if @message.save
      flash[:success] = "Messaggio inviato"
      redirect_to search_users_path(params[:message][:group_id])
    else
      render :action => :new
    end
  end

  def show
    @message = Message.find(params[:id])
  end  

  def destroy
    @message = Message.find(params[:id])
    @message.destroy
    redirect_to user_path(current_user.id) 
  end

private

  def correct_user
      @message = current_user.messages.where(:recipient_id => current_user)
      redirect_to root_path if @message.nil?
  end
end
