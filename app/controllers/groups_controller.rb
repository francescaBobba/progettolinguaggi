class GroupsController < ApplicationController
  before_filter :signed_in_user, only: [:create]
  before_filter :correct_user,   only: :destroy
  
  def show
    @group = Group.find(params[:id])
    @micropost  = current_user.microposts.build
    @user = current_user
    @feed_items = current_user.feed.paginate(page: params[:page])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def index
   @user = current_user
   @groups= Group.all
  end
  
  def create
    @group = Group.new(params[:group])
    @group.admin = current_user.id
    if @group.save
      @groupship = Groupship.new
      @groupship.user_id = current_user.id
      @groupship.group_id= @group.id
      @groupship.save
# Handle a successful save
      flash[:success] = "Gruppo creato!"
      redirect_to search_users_path(@group.id)
    else
      redirect_to groups_path
    end
  end


  def destroy
    @group.destroy
    redirect_to groups_path 
  end
  
  
   private

	def correct_user
      @group = current_user.groups.find_by_id(params[:id])
      redirect_to root_path if @group.nil?
  end
end