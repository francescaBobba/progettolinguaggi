class UsertagsController < ApplicationController
    
  def new
    @usertag = Usertag.new
    @user = current_user
    @tags = Tag.all
    @usertags = current_user.usertags.build
  end

  def create
   @usertag = Usertag.new(params[:usertag])
   @usertag.user_id = current_user.id

   if @usertag.save
      flash[:success] = "usertag creato!"
      redirect_to new_usertag_path 
    else
      redirect_to tags_path
   end
  end
end
