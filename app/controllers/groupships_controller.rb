class GroupshipsController < ApplicationController

  def new
    @user = current_user
    @group = Group.find(params[:group_id])
    @groupship = Groupship.new
    @groupships = current_user.groupships.build
  end
  
  def create
   @groupship = Groupship.new(params[:groupship])
   @groupship.user_id = current_user.id
   if @groupship.save
      flash[:success] = "groupship creato!"
      redirect_to groups_path
    else
      redirect_to groups_path
   end
  end
  
  def destroy
    @groupship = Groupship.find(params[:id])
    @groupship.destroy
    redirect_to groups_path
  end
  

end