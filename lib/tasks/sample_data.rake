namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    99.times do |n|
      id = n
      name  = Faker::Name.name
      email = "s#{n+1}@polito.it"
      password  = "password"
      hobby = "Sport estremi, Dungeons&Dragons"
      work = "Tirocinio curricolare svolto nel periodo da settembre 2007 a gennaio 2008 presso TaldeiTali Spa, via del Comune 0, Torino.
Durante il tirocinio ho approfondito i concetti di base di argomenti che non mi interessavano e che sono stati affrontati senza interesse." 

      skill = "Web: HTML/CSS, Javascript, JSP, Ruby on Rails
Programmazione a oggetti: Java, Android
Gestione dell'acustica di piccoli ambienti"


      training = "Si forma al Liceo Scientifico 'Amedeo Avogadro', in cui consegue il diploma nel 1994.
Successivamente si iscrive al corso di laurea triennale in Ingegneria del Cinema e dei Mezzi di Comunicazione presso il Politecnico di Torino.
Consegue la laurea triennale nel 2008 (102/110).
Dopo la laurea triennale, si iscrive alla laurea magistrale in Ingegneria del Cinema e dei Mezzi di Comunicazione, di cui prevedere di prendere il titolo a breve."

      User.create!(
                   id: id,
                   name: name,
                   email: email,
                   password: password,
                   training: training,
                   skill: skill,
                   work: work,
                   hobby: hobby 
                   )
    end
  end
end